<?php

class Kontakt_Admin_Controller extends Admin_Controller {

    public function index() {
        $this->view->activeNavigation = 'kontakt';
        $contacts = $this->model->getContacts();
        $this->view->contacts = $contacts;
        $this->view->render('kontakt/kontakti.php');
    }

    public function obrisiKontakt() {
        $contactId = $_GET['contact_id'];
        $this->model->deleteContact($contactId);
        header('Location: ' . ADMIN_URL . 'kontakt');
    }
    
    public function kontaktStranica(){
        $this->view->activeNavigation = 'kontaktStranica';
        $kontaktPage = $this->model->getKontaktPage();
        $this->view->kontaktPage = $kontaktPage;
        $this->view->render('kontakt/kontakt_stranica.php');
    }
    
    public function azurirajKontaktStranicu() {
        $title = $_POST['title'];
        $description = $_POST['description'];
        
        $this->model->updateContactPage($title, $description);
        
        header('Location: ' . ADMIN_URL . 'kontakt/kontaktStranica?message=azurirana');
    }

    public function odgovori($contactId) {
        $contact = $this->model->getContact($contactId);
        $this->view->contact = $contact;
        $this->view->render('kontakt/odgovorKontakt.php');
    }

}

?>