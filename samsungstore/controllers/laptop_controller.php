<?php

class Laptop_Controller extends Controller {

	public function index() {
		$this->view->activeNavigation = 'laptop';

		$laptop = $this->model->getLaptop();
		$this->view->laptop = $laptop;
		$this->view->render('laptop/laptop.php');
	}
}
