<?php

class Tv_Controller extends Controller {

	public function index() {
		$this->view->activeNavigation = 'tv';

		$tv = $this->model->getTv();
		$this->view->tv = $tv;
		$this->view->render('tv/tv.php');
	}
}