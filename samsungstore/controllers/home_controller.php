<?php

class Home_Controller extends Controller {

	public function index() {
		$products = $this->model->getProducts();
		$this->view->products = $products;
		$this->view->render('home/index.php');
	}

	public function sortAtoZ() {
		$_SESSION['sort'] = 'ASC';

		$products = $this->model->sortProduct();
		$this->view->products = $products;
		$this->view->render('home/index.php');
	}

	public function sortZtoA() {
		$_SESSION['sort'] = 'DESC';

		$products = $this->model->sortProduct();
		$this->view->products = $products;
		$this->view->render('home/index.php');
	}

	public function product($id) {
		$product = $this->model->getProduct($id);
		$this->view->product = $product;
		$this->view->render('home/product.php');
	}

}