<?php

class Phone_Controller extends Controller {

	public function index() {
		$this->view->activeNavigation = 'phone';

		$phones = $this->model->getPhones();
		$this->view->phones = $phones;
		$this->view->render('phone/phone.php');
	}
}
