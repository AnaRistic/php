<?php

class Home_Admin_Controller extends Admin_Controller {

	public function index() {
		$this->view->activeNavigation = 'home';

		$products = $this->model->getProducts();
		$this->view->products = $products;
		$this->view->render('samsung/home.php');
	}

	public function edit($id) {
		$product = $this->model->getProduct($id);
		$this->view->product = $product;
		$this->view->render('samsung/edit_product.php');
	}

	public function editProduct() {

		$id = $_POST['id'];

		$product = $this->model->update($id);
		header('Location: '.ADMIN_URL.'home');
	}

	public function delete($id) {

		$product = $this->model->deleteProduct($id);
		header('Location: '.ADMIN_URL.'home');
	}

	public function newProduct() {
		$this->view->activeNavigation = 'newProduct';

		$this->view->render('samsung/new_product.php');
		
	}

	public function addProduct() {
		$type = $_POST['type'];
		$model = $_POST['model'];
		$description = $_POST['description'];
		$price = $_POST['price'];
		$available = isset($_POST['available'])?$_POST['available']:0;

		$product = $this->model->addProduct($type, $model, $description, $price, $available);
		header("Location: ".ADMIN_URL."home/newProduct?msg=update");
	}
		

}