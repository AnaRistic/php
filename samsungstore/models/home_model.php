<?php

include_once LIBS . 'Admin_Model.php';
include_once BASE_PATH . 'models/home_admin_model.php';

class Home_Model extends Home_Admin_Model {

	public function sortProduct() {

		$sql = "SELECT * FROM samsung_products ORDER BY model {$_SESSION['sort']}";
		$result = $this->db->query($sql);

		if ($result->rowCount() > 0) {
			$products = $result->fetchAll(PDO::FETCH_ASSOC);
		}
		
		return $products;
	}

}