<?php

class Home_Admin_Model extends Admin_Model {

	public function getProducts() {

		$sql = "SELECT * FROM samsung_products";
		$result = $this->db->query($sql);

		if ($result->rowCount() > 0) {
			$products = $result->fetchAll(PDO::FETCH_ASSOC);
		}
		return $products;
	}

	public function getProduct($id) {

		$sql = "SELECT * FROM samsung_products WHERE id = $id";
		$result = $this->db->query($sql);

		if ($result->rowCount() > 0) {
			$product = $result->fetch(PDO::FETCH_ASSOC);
		}
		return $product;
	}

	public function update($id) {

		$type = $_POST['type'];
		$model = $_POST['model'];
		$description = $_POST['description'];
		$price = $_POST['price'];
		$available = $_POST['available'];

		$sql = "UPDATE samsung_products SET type = '$type', model = '$model', description = '$description', price = '$price', available = '$available' WHERE id = $id";
		$result = $this->db->query($sql);

		return true;
	}

	public function deleteProduct($id) {

		$sql = "DELETE FROM samsung_products WHERE id = $id";
		$result = $this->db->query($sql);

		return true;
	}

	public function addProduct($type, $model, $description, $price, $available) {

		$sql = "INSERT INTO samsung_products (type, model, description, price, available)
				VALUES ('$type', '$model', '$description', $price, $available)";
		$result = $this->db->query($sql);

		return true;
	}

}