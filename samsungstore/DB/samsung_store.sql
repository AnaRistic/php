-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2019 at 06:41 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `samsung_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `samsung_products`
--

CREATE TABLE IF NOT EXISTS `samsung_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` set('phone','tv','laptop') CHARACTER SET utf8 NOT NULL,
  `model` varchar(10) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf32 NOT NULL,
  `price` double NOT NULL,
  `available` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `samsung_products`
--

INSERT INTO `samsung_products` (`id`, `type`, `model`, `description`, `price`, `available`) VALUES
(1, 'phone', 'S5670 Gala', 'Veličina ekrana: 3.3 inča Rezolucija ekrana: 320x240 piksela Radna memorija: 280MB Interna memorija: 160MB', 13999, 1),
(2, 'phone', 'Galaxz S4 ', 'Veličina ekrana 4.3 inča, Baterija Li-lon 1900 mAh', 50000, 1),
(3, 'phone', 'i9300 Gala', 'Veličina ekrana 4.8 inča, Rezolucija ekrana 1280x720 piksela, Interan memorija 16GB', 49500, 1),
(4, 'tv', '32 LED UE ', 'Dijagonala 32" (81cm) Rezolucija 1366x768 (HD Readz)', 34999, 1),
(5, 'tv', '40 LED Sma', 'Rezolucija: 1920x1080 (Full HD) Frekvencija: 200Hz', 70000, 1),
(6, 'laptop', 'Ultrabook ', 'Memorija: 4GB DDR3 1600 MHz, Ekran: 14" HD LED 1366x768', 80000, 1),
(7, 'laptop', 'ATIV Book2', 'Memorija 2GB (1x2GB) DDR3 1333MHz, hard disk 320GB SATA 5400rpm', 27000, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
