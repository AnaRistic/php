<div class="items">

	<ul class="items_list cf">

		<?php foreach ($this->tv as $tv) { 
			if ($tv['available'] == 1 && $tv['type'] == 'tv') { ?>
				<li>
					<div class="item_thumb">
						<img src="<?php echo URL.'images/proizvodi/tv.png'; ?>">
					</div>
					<a><h2 class="title4"><?php echo $tv['type'].' | '.$tv['model']; ?></h2></a>
				</li>
			<?php 
			} else { 
					echo "";
				}
		} ?>
	
	</ul>

</div>