<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
        <title>Samsun Store</title>
        <link rel="stylesheet" href="<?php echo URL . 'views/public/style.css'; ?>"/>
        <script type="text/javascript" src="<?php echo URL . 'libs/javascript/jquery-1.10.2.min.js' ?>"></script>
    </head>
    <body>
        
        <div id="page">
            <div class="header cf">
                
                <div class="logo">
                    <img id="img" src="<?php echo URL.'images/samsung.png' ?>"> STORE
                </div>

                
            </div>
            
            <div class="navigation cf">
                <ul>
                    <li><a <?php if (!empty($this->controllerName) && $this->controllerName == 'home') { echo 'class="active"' ; } ?> href="<?php echo URL . 'home' ?>">SAMSUNG</a></li>
                    <li><a <?php if (!empty($this->controllerName) && $this->controllerName == 'phone') { echo 'class="active"' ; } ?> href="<?php echo URL . 'phone' ?>">PHONE</a></li>
                    <li><a <?php if (!empty($this->controllerName) && $this->controllerName == 'tv') { echo 'class="active"' ; } ?> href="<?php echo URL . 'tv' ?>">TV</a></li>
                    <li><a <?php if (!empty($this->controllerName) && $this->controllerName == 'laptop') { echo 'class="active"' ; } ?> href="<?php echo URL . 'laptop' ?>">LAPTOP</a></li>
                    <li style="float:right"><a href="<?php echo ADMIN_URL . 'home' ?>">ADMIN</a></li>
                </ul>
            </div>
            <div class="content">

