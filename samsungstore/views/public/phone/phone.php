<div class="items">

	<ul class="items_list cf">

		<?php foreach ($this->phones as $phone) { 
			if ($phone['available'] == 1 && $phone['type'] == 'phone') { ?>
				<li>
					<div class="item_thumb">
						<img src="<?php echo URL.'images/proizvodi/phone.png'; ?>">
					</div>
					<a><h2 class="title4"><?php echo $phone['type'].' | '.$phone['model']; ?></h2></a>
				</li>
			<?php 
			} else { 
					echo "";
				}
		} ?>
	
	</ul>

</div>