<div class="items">

	<span id="sort"><a href="<?php echo URL.'home/sortAtoZ' ?>">A-Z</a> | <a href="<?php echo URL.'home/sortZtoA' ?>">Z-A</a></span>

	<ul class="items_list cf">
	<?php foreach ($this->products as $product) { 
		if ($product['available'] == 0) {
			echo "";
		} else { ?>
		<li>
			<div class="item_thumb">
					<?php 
					if ($product['type'] == 'phone') {
						$image = 'phone.png';
						}
		
					elseif ($product['type'] == 'laptop') {
						$image = 'laptop.png';
					}
		
					elseif ($product['type'] == 'tv') {
						$image = 'tv.png';
					} 
		
					else { 
						$image = 'no_image.png'; };
					?>
				<a href="<?php echo URL.'home/product/'.$product['id'] ?>"><img src="<?php echo URL.'images/proizvodi/'.$image; ?>"></a>
			</div>
			<a href="<?php echo URL.'home/product/'.$product['id'] ?>"><h2 class="title4"><?php echo $product['type'].' | '.$product['model']; ?></h2></a>
		</li>
	<?php } 
	}?>
		
	</ul>

</div>