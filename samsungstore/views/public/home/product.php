<?php 
if ($this->product['type'] == 'phone') {
	$image = 'phone.png';
}

elseif ($this->product['type'] == 'laptop') {
	$image = 'laptop.png';
}

elseif ($this->product['type'] == 'tv') {
	$image = 'tv.png';
}

else { 
	$image = 'no_image.png';
}

?>

<img src="<?php echo URL.'images/proizvodi/'.$image; ?>" id="productimg">

<div class="item_detail">
	<h1><?php echo $this->product['model'] ?></h1>

	<p>
		<?php echo $this->product['description']; ?>
	</p>

	<div class="price">
		<?php echo number_format($this->product['price'], 2, ',', '.'); ?> RSD
	</div>
</div>

