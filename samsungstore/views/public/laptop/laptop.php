<div class="items">

	<ul class="items_list cf">

		<?php foreach ($this->laptop as $laptop) { 
			if ($laptop['available'] == 1 && $laptop['type'] == 'laptop') { ?>
				<li>
					<div class="item_thumb">
						<img src="<?php echo URL.'images/proizvodi/laptop.png'; ?>">
					</div>
					<a><h2 class="title4"><?php echo $laptop['type'].' | '.$laptop['model']; ?></h2></a>
				</li>
			<?php 
			} else { 
					echo "";
				}
		} ?>
	
	</ul>

</div>