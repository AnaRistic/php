<h1>Edit product</h1>

<form class="form" action="<?php echo ADMIN_URL.'home/editProduct'; ?>" method="post">
	<input type="hidden" name="id" value="<?php echo $this->product['id']; ?>" >
	<label for="type">Type: </label><select name="type">
		<option value="">-------</option>
		<option value="phone" <?php echo $this->product['type']=='phone'?'selected':'' ?>>phone</option>
		<option value="tv" <?php echo $this->product['type']=='tv'?'selected':'' ?> >tv</option>
		<option value="laptop" <?php echo $this->product['type']=='laptop'?'selected':'' ?> >laptop</option>
	</select>
	<br/>
	<label for="model">Model: </label><input type="text" name="model" value="<?php echo $this->product['model'] ?>">
	<br/>
	<label for="description">Description: </label><textarea name="description"><?php echo $this->product['description'] ?></textarea>
	<br/>
	<label for="price">Price: </label><input type="number" name="price" value="<?php echo $this->product['price'] ?>">
	<br/>
	<label for="checkbox">Available: </label><input type="checkbox" name="available" value="1" <?php echo $this->product['available']=='1'?'checked':'' ?> >
	<br/>
	<input type="submit" value="UPDATE">
</form>