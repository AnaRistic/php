<h1>Samsung</h1>

<table class="mt15" border="1">
	<tr>
		<th>ID</th>
		<th>Type</th>
		<th>Model</th>
		<th>Description</th>
		<th>Price</th>
		<th>Available</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>

	<?php foreach ($this->products as $product) { ?>

		<tr>
			<td><?php echo $product['id']; ?></td>
			<td><?php echo $product['type']; ?></td>
			<td><?php echo $product['model']; ?></td>
			<td><?php echo $product['description']; ?></td>
			<td><?php echo number_format($product['price'], 2, ',', '.'); ?> RSD</td>
			<td><?php echo $product['available'] == 1 ? "yes" : "no"; ?></td>
			<td><a href="<?php echo ADMIN_URL.'home/edit/'. $product['id']; ?>"><img class="edit_img" src="<?php echo URL.'images/edit.png' ?>"></a></td>
			<td><span class="deleteItem" id="<?php echo $product['id']; ?>"><img class="delete_img" src="<?php echo URL.'images/icon_delete.png' ?>"></span></td>
		</tr>
		
	<?php } ?>

</table>

<div class="background">

	<div id="question">
		<div id="btnClose"><img src="<?php echo URL.'images/icon_close.png'; ?>"></div>
		Da li ste sigurni da želite da obrišete proizvod?
		<div class="buttons">
			<div id="btnYes" class="button">
				<a href="">Da</a>
			</div>
			<div id="btnNo" class="button">Ne</div>
		</div>
	</div>
	
</div>

<script>
	jQuery(document).ready(function($){

		$('.deleteItem').click(function(){
			$('.background').fadeIn('fast');
			var id = $(this).attr('id');
			var link = "<?php echo ADMIN_URL.'home/delete/'; ?>" + id;
			$('#btnYes a').attr('href', link);
		});

		function closeBackground(speed) {
			$('.background').fadeOut(speed);
		}
		$('#btnClose').click(function(){
			closeBackground('fast');
		});
		$('#btnNo').click(function(){
			closeBackground('fast');
		});
	});
</script>