<h1>New product</h1>

<form class="form" action="<?php echo ADMIN_URL.'home/addProduct' ?>" method="post">
	<label for="type">Type: </label><select name="type">
		<option value="">-------</option>
		<option value="phone">phone</option>
		<option value="tv">tv</option>
		<option value="laptop">laptop</option>
	</select>
	<br/>
	<label for="model">Model: </label><input type="text" name="model">
	<br/>
	<label for="description">Description: </label><textarea name="description"></textarea>
	<br/>
	<label for="price">Price: </label><input type="number" name="price">
	<br/>
	<label for="checkbox">Available: </label><input type="checkbox" name="available" value="1">
	<br/>
	<input type="submit" value="ADD">
</form>