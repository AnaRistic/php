<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
        <title>Samsunt Store - Administration</title>
        <link rel="stylesheet" href="<?php echo URL . 'views/admin/style_admin.css'; ?>"/>
        <script type="text/javascript" src="<?php echo URL . 'libs/javascript/jquery-1.10.2.min.js' ?>"></script>
    </head>
    <body>
        <div id="page">
            <div class="header cf">
                
                <div class="logo">Samsung Store - Administration</div>

            </div>
            
            <div class="navigation cf">
                <ul>
                    <li><a <?php if (!empty($this->activeNavigation) && $this->activeNavigation == 'home') { echo 'class="active"' ; } ?> href="<?php echo ADMIN_URL . 'home' ?>">Samsung</a></li>
                    <li><a <?php if (!empty($this->activeNavigation) && $this->activeNavigation == 'newProduct') { echo 'class="active"' ; } ?> href="<?php echo ADMIN_URL . 'home/newProduct' ?>">New product</a></li>
                    <li style="float:right"><a href="<?php echo URL . 'home' ?>">Samsung store</a></li>  
                </ul>
            </div>
            <div class="content">

